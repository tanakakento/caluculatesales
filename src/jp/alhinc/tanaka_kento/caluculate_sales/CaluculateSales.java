package jp.alhinc.tanaka_kento.caluculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

class BranchList {

public static class CaluculateSales {


	public static void main(String[] args) {
		try {
			Map<String , String> branchName = new HashMap<String , String>();
			Map<String , Long> branchSales = new HashMap<String , Long>();
			BufferedReader br = null;
			try {
				File file = new File(args[0], "branch.lst");
				if(file.exists()) {
					br = new BufferedReader(new FileReader(file));

					String line;
					while((line = br.readLine()) != null) {

						String[] lineSprit = line.split(",");
						if(lineSprit[0].matches("[0-9]{3}")) {
							if(lineSprit.length == 2) {
								branchName.put(lineSprit[0] , lineSprit[1]);
							} else {
								System.out.println("支店定義ファイルのフォーマットが不正です");
								return;
							}
						} else {
							System.out.println("支店定義ファイルのフォーマットが不正です");
							return;
						}
					}
				} else {
					System.out.println("支店定義ファイルが存在しません");
					return;
				}
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				} finally {
					if(br != null) {
						try {

							br.close();
						} catch(IOException e) {
							System.out.println("予期せぬエラーが発生しました");
							return;
						}
					}
				}

		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File file , String str) {
				if(str.matches("[0-9]{8}.rcd")) {
					return true;
				} else {
					return false;
				}
			}
		};

		File dir = new File(args[0]);
		File[] list = dir.listFiles(filter);
		String[] listName = new String[list.length];
		for(int i = 0 ; i < list.length ; i++) {
			listName[i] = list[i].getName();
		}
		if(Integer.parseInt(listName[0].substring(0 , 8)) + (list.length -1) != Integer.parseInt(listName[list.length -1].substring(0 , 8))) {
			System.out.println("売上ファイルが連番になっていません");
			return;
		}
		String[] key = new String[list.length];
		Long[] sales = new Long[list.length];
		if(list != null) {
			for(int i = 0 ; i < list.length ; i++) {

				try {
					FileReader fr = new FileReader(list[i]);
					br = new BufferedReader(fr);
					String line ;
					int lineCount = 0;
					while((line = br.readLine()) != null) {
						lineCount++;
						if(lineCount > 2) {
							System.out.println(list[i].getName() + "のフォーマットが不正です");
							return;
						}
						if(lineCount == 1) {
							key[i] = line;
							if (branchName.containsKey(key[i]) != true) {
								System.out.println(list[i].getName() + "の支店コードが不正です");
								return;
							}
						}
						if(lineCount == 2) {
							sales[i] = Long.parseLong(line);
						}
					}
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return;
				} finally {
					if(br != null) {
						try {
							br.close();
						} catch(IOException e) {
							System.out.println("予期せぬエラーが発生しました");
							return;
						}
					}
				}
			}
		}
		for(int i = 0 ; i < key.length ; i++) {
			long value;
			if (branchSales.containsKey(key[i]) != true) {
				branchSales.put(key[i], sales[i]);
			} else {
				value = branchSales.get(key[i]);
				value = value + sales[i];
				if(String.valueOf(value).length() > 10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				branchSales.put(key[i], value);
			}
		}
		try{
			File file = new File(args[0] , "branch.out");

			if (checkBeforeWriteFile(file)){
				PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(file)));
				for (String code : branchName.keySet()) {
					if(branchSales.get(code) == null) {
						pw.println(code + "," + branchName.get(code) + ",0");
					} else {
						pw.println(code + "," + branchName.get(code) + "," + branchSales.get(code));
					}
		        }
				pw.close();
		      } else {
		    	  System.out.println("予期せぬエラーが発生しました");
		    	  return;
		      }
		 	} catch(IOException e){
		 		System.out.println("予期せぬエラーが発生しました");
		 		return;
		    }
		} catch(Exception e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}
}

		private static boolean checkBeforeWriteFile(File file) {
		    if (file.exists()) {
		      if (file.isFile() && file.canWrite()) {
		        return true;
		      }
		    }
		    return false;
		}

}





